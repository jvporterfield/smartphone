﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPhoneJP
{
  class Specs
  {
    /// <summary>
    /// construtor with default spec values
    /// </summary>
    public Specs()
    {
      make = "LG";
      model = "L38C";
      screenSize = "3 inches x 4.5 inches";
      screenRes = "3.2";
      batteryCap = "2.5 days";
      processor = "800Mhz";
    }

    public string make { get; set; }
    public string model { get; set; }
    public string screenSize { get; set; }
    public string screenRes { get; set; }
    public string batteryCap { get; set; }
    public string processor { get; set; }
  }

}
