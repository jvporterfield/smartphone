﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPhoneJP
{
  class Storage
  { 
    public Storage()
    {
      storageCap = 1;
      extraStorage = 3.69m;
    }

    public int storageCap {get; set;}
    public decimal extraStorage {get; set;}

    public bool HasextraStorage
    {
      if (extraStorage.HasValue)
    {
      return true;
    }
      else
    {
      return false;
    }
    }
}
}
