﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartPhoneJP
{
  class Camera
  {
    public Camera()
    {
      camRes = "3.2MP";
      cam2Res = "No Second Camera";
      camZoom = "Yes";
    }

    public string camRes { get; set; }
    public string cam2Res { get; set; }
    public string camZoom { get; set; }

    public bool HasCamera
     { 
      if (res.HasValue)
      {
        return true;
      }

      else
     {
       return false;
     }
    }
  }
}
